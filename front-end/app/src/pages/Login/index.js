import React, {useState} from 'react';
import {Link, useHistory} from 'react-router-dom';

import './style.css';
import owlImage from '../../assets/owl-hour-icone.svg';

import api from '../../services/api';



export default function Login(){
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const history = useHistory('');
    
    async function handleLogin(event){
        event.preventDefault();
        
        try{
            
            history.push('/profile');

        }catch(error){
            alert("Error, try again.")
        }

        // console.log(email, password);
    }
    
    return(
        
        <div className="login-container">
            <section className="left-section">
                <div className="logo-image">
                    <img src={owlImage} alt="orange owl"/>
                </div>
                <div className="card-mensage">
                    <p>Hello!</p>
                    <p>Take your time.</p>
                </div>
            </section>
            <section className="form-section">
                <form onSubmit={handleLogin}>
                    <label>Email</label>
                    <input type="email"
                            value={email} 
                            placeholder="Digite seu email"
                            onChange={event => setEmail(event.target.value)}
                    />
                    <label>Password</label>
                    <input type="password" 
                           value={password} 
                           placeholder="Digite sua senha"
                           onChange={event=> setPassword(event.target.value)}
                    />
                    <button type="submit" className="button-login">Login</button>
                </form>

            </section>
        </div>   
    );
}