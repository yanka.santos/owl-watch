import React, { useState } from 'react';
import {Link, useHistory} from 'react-router-dom';
// import Tooltip from 'react-tooltip-lite';

import './style.css';
import logo from '../../assets/logo-owl-mini.svg';
import tecnologia from '../../assets/tecnologia.svg';
import clock from '../../assets/despertador.svg'
import medal from '../../assets/medal.svg';

export default function Login(event){
    
    const [time, setTime] = useState('');
    const [description, setDescription] = useState('');
    const [date, setDate] = useState('');
    const [hourBalance, setHourBalance] = useState('');

    const history = useHistory('');
    
    async function handleProfile(event){
        event.preventDefault();
        
        const time = [];
        const description = [];
        
        const hour = parseInt(time);
        // hourBalance = hour -= 8;

        try{
            localStorage.setItem(time, time);
            localStorage.setItem(description, description);
            
            hourBalance = -5

            // const totaltime = time.reduce((total, time) => {
            //     if(time.type >= 0) return total
            
            //     return total + time
            // },0)
            
            


         

        }catch(error){
            alert("Error, try again.")
        }

        
    }

    return (
       <div className="container-profile">
           <nav>
               <img src={logo} alt="hand of owl orange"></img>
           </nav>
           <section className="hour-note">
               <form onSubmit={handleProfile}>
                   <img src={tecnologia} alt="touch screen"/>
                   <div className="input-section">
                        <label>Time: </label>
                        <input type="text"
                                value={time}
                                placeholder="00:00"
                                onChange={event => setTime(event.target.value)}
                        />
                        {/* <Tooltip className="tooltip-content" content="set current time">
                            <img src={clock} alt="set current time"/>
                        </Tooltip> */}
                        <label id="description">Description: </label>
                        <input type="text"
                            value={description}
                            placeholder="insert description"
                            onChange={event => setDescription(event.target.value)}
                        />
                   </div>                                                        
                    <button type="submit">Register hour</button>
               </form>
               <div className="container-block">
                   <div className="block-list-hour">
                        <h1>List of records</h1>
                        <ul>
                                <li>{[time] + "  " + [description] }</li>
                        </ul>
                   </div>
                   <div className="block-options">
                       <h1>List options</h1>
                       <ul>
                           <li>
                               <img src={medal} alt="medal option Adjustement"/>
                               Adjustement
                           </li>
                           <li>
                               <img src={medal} alt="medal option Allowance"/>
                               Allowance
                           </li>
                       </ul>
                       <div className="content-hour-balance">
                           <h2>Hour balance</h2>
                            <p className="negative">{ hourBalance}</p>
                            <img className="clock-icon" src={clock} alt="clock image"/>
                       </div>
                       
                   </div>
               </div>
           </section>
       </div>



    );
}
