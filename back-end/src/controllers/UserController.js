const connection = require("../database/connection");
const crypto = require("crypto");

module.exports = {
    
    async createUser(request, response) {
        const {name, lastName, email} = request.body;

        const password = crypto.randomBytes(6).toString('HEX');

        await connection('users').insert({
            name,
            lastName,
            email,
            password
        })

        return response.json({password});
    },

    async listUser(request, response){
        const users = await connection('users').select('*');

        return response.json(users)
    }


};