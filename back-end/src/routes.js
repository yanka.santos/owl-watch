const express = require('express');
const timeRecordController = require('./controllers/TimeRecordController');
const userController = require('.//controllers/UserController');

const routes = express.Router();

routes.get('/users', userController.listUser);
routes.post('/users', userController.createUser);


module.exports= routes;
