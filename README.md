# Owl Watch

![Alt](./front-end/app/src/assets/logo-owl-mini.svg "owl orange")



### Projeto realizado para ponto eletrônico de funcionários.

A plataforma será acessada por funcionários da empresa Owl Watch,
onde o funcionário irá realizar o processo de login inserindo seu email corporativo,
e a senha que o departamento pessoal lhe enviou por email.


[Protótipo do projeto](https://www.figma.com/file/R1GKisPswf7b7ndKbsD6zY/Owl-Hour?node-id=9%3A97 "figma - owl watch prototipo")

## As funcionalidades a serem desenvolvidas são:

> * Formulário de login, capturando os valores passados pelo input;
> * Roteamento para tela do perfil;
> * Formulário onde o funcionário registra a hora e uma descrição;
> * Box de listagem de horas apondadas;
> * Box de opções e saldo de horas;

## Funcionalidades futuras:

> * Tela de cadastro de usuário;
> * Modal de ajuste de apontamento de hora;
> * Modal de abono de horas;
> * Responsividade para mobile;
> * Implementação de testes;


Para rodar o projeto na sua máquina é preciso que você digite
no seu terminal, de preferencia numa pasta a sua escolha.

> `$ git clone https://gitlab.com/yankasantos0107/owl-watch.git`

depois de dar enter e ele baixar todos os arquivos, [installe o node](https://nodejs.org/en/) no seu computador, após isso pode digitar:

 **back-end**
> `$ cd ./back-end`
> `$ npm install`

 **front-end**
> `$ cd ../front-end/app`
> `$ npm install`

ele vai fazer o download de todas as dependências do projeto;

Feito isso para dar o start no servidor back-end, é preciso entrar na pasta back-end pelo terminal e digitar:

> `$ npm start`

ele vai mostrar que a aplicação está rodando no terminal ou caso acesse a url 
> http://localhost:3333

para o front-end você replica as mesmas ações do back-end só que a tela ela abri assim que levanta o servidor, mas caso não ocorra pode acessar digitando no browser
> http://localhost:3000/


